//
//  AppDelegate.h
//  Tune-O-Matic
//
//  Created by Cristiano Tenuta on 10/7/14.
//  Copyright (c) 2014 Cubique Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

