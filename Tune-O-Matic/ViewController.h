//
//  ViewController.h
//  Tune-O-Matic
//
//  Created by Cristiano Tenuta on 10/7/14.
//  Copyright (c) 2014 Cubique Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EZMicrophone.h>

@interface ViewController : UIViewController <EZMicrophoneDelegate>

@property (weak, nonatomic) IBOutlet UIImageView  *switchOn;
@property (weak, nonatomic) IBOutlet UIImageView  *switchOff;


@property (weak, nonatomic) IBOutlet UIImageView  *indicator;
@property (weak, nonatomic) IBOutlet UIImageView  *arrow;

@property (weak, nonatomic) IBOutlet UILabel  *noteValue;
@property (weak, nonatomic) IBOutlet UILabel  *frequencyValue;
@property (weak, nonatomic) IBOutlet UILabel  *centsValue;

@property (weak, nonatomic) IBOutlet UILabel  *frequency;
@property (weak, nonatomic) IBOutlet UILabel  *cents;
@property (weak, nonatomic) IBOutlet UILabel  *buttonState;

@property (strong,nonatomic) EZMicrophone *microphone;
@end
