//
//  ViewController.m
//  Tune-O-Matic
//
//  Created by Cristiano Tenuta on 10/7/14.
//  Copyright (c) 2014 Cubique Solutions. All rights reserved.
//

#import "ViewController.h"
#import <math.h>
#import "TPCircularBuffer.h"
#import <AVFoundation/AVAudioSession.h>
#import <sys/sysctl.h>

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface ViewController () {

TPCircularBuffer circularBuffer;
}
@end


@implementation ViewController

float pitchValid;
float lastPitch;
//float *sampleBuffer;
float pitchProbability;

bool isRecording = false;
bool isPitched = false;

int currentAngle;
int lastAngle;
int sampleCount;

int lastNoteIndex;
NSDate *lastPitchTime;

NSArray *noteNames;

//NSTimer *calculatPitchTimer;
//NSTimer *updateInterfaceTimer;
CABasicAnimation* animation;



int bufferSize = 2048;
NSUInteger channels = 2;
double sampleRate = 44100.0;


- (void)viewDidLoad {
    [super viewDidLoad];

    noteNames = [NSArray arrayWithObjects:
                 @"C0",@"C#0",@"D0",@"Eb0",@"E0",@"F0",@"F#0",@"G0",@"Ab0",@"A0",@"Bb0",@"B0",
                 @"C1",@"C#1",@"D1",@"Eb1",@"E1",@"F1",@"F#1",@"G1",@"Ab1",@"A1",@"Bb1",@"B1",
                 @"C2",@"C#2",@"D2",@"Eb2",@"E2",@"F2",@"F#2",@"G2",@"Ab2",@"A2",@"Bb2",@"B2",
                 @"C3",@"C#3",@"D3",@"Eb3",@"E3",@"F3",@"F#3",@"G3",@"Ab3",@"A3",@"Bb3",@"B3",
                 @"C4",@"C#4",@"D4",@"Eb4",@"E4",@"F4",@"F#4",@"G4",@"Ab4",@"A4",@"Bb4",@"B4",
                 @"C5",@"C#5",@"D5",@"Eb5",@"E5",@"F5",@"F#5",@"G5",@"Ab5",@"A5",@"Bb5",@"B5",
                 @"C6",@"C#6",@"D6",@"Eb6",@"E6",@"F6",@"F#6",@"G6",@"Ab6",@"A6",@"Bb6",@"B6",
                 @"C7",@"C#7",@"D7",@"Eb7",@"E7",@"F7",@"F#7",@"G7",@"Ab7",@"A7",@"Bb7",@"B7",
                 @"C8",@"C#8",@"D8",@"Eb8",@"E8",@"F8",@"F#8",@"G8",@"Ab8",@"A8",@"Bb8",@"B8",
                 @"C9",@"C#9",@"D9",@"Eb9",@"E9",@"F9",@"F#9",@"G9",@"Ab9",@"A9",@"Bb9",@"B9",nil ];
    
    lastAngle = 0;
    
    
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(enteredBackround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    
    [self initInterface];
    
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    NSLog(@"iPhone Device%@",[self platformType:platform]);
    free(machine);
    
    if ([[self platformType:platform] hasPrefix:@"iPhone 6s"])
        sampleRate = 48000;
    
    [self initAudioSession];

}

- (void) initAudioSession
{
    
    NSError *error;
    
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];

    [[AVAudioSession sharedInstance] setPreferredSampleRate:sampleRate error:&error];
    [[AVAudioSession sharedInstance] setPreferredInputNumberOfChannels:2 error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    [EZMicrophone sharedMicrophone].delegate = self;
    [[EZMicrophone sharedMicrophone] setDevice:[[EZAudioDevice inputDevices] lastObject]];

}


- (void)enteredBackround:(NSNotification*) not
{
    _switchOff.hidden = NO;
    _switchOn.hidden = YES;
    

    [_buttonState setText:@"OFF"];
    
    
    [[EZMicrophone sharedMicrophone] stopFetchingAudio];
    
    isRecording = false;
    
    // Release buffer resources
    TPCircularBufferCleanup(&circularBuffer);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    _switchOff.hidden = NO;
    _switchOn.hidden = YES;
    
    
    [_buttonState setText:@"OFF"];
    
    
    [[EZMicrophone sharedMicrophone] stopFetchingAudio];
    
    
    isRecording = false;
    
    TPCircularBufferCleanup(&circularBuffer);

}

- (IBAction)didTapSwitchOn:(id)sender
{
    
    //[self initAudioSession];
    
    TPCircularBufferInit(&circularBuffer, bufferSize * sizeof(float));
    
    _switchOff.hidden = YES;
    _switchOn.hidden = NO;
    
    
    sampleCount = 0;
    lastNoteIndex = 0;
    lastPitchTime = nil;
    
    [[EZMicrophone sharedMicrophone] startFetchingAudio];
    
    isRecording = true;
    
    [_buttonState setText:@"ON"];
    
    
}


- (IBAction)didTapSwitchOff:(id)sender
{
    _switchOff.hidden = NO;
    _switchOn.hidden = YES;
    

    [_buttonState setText:@"OFF"];
    
    
    [[EZMicrophone sharedMicrophone] stopFetchingAudio];
    
    
    isRecording = false;
    
    TPCircularBufferCleanup(&circularBuffer);

    
}


//- (void) microphone:(EZMicrophone *)microphone
//   hasAudioReceived:(float **)buffer
//     withBufferSize:(UInt32)bufferSize
//withNumberOfChannels:(UInt32)numberOfChannels {
//    // Getting audio data as an array of float buffer arrays that can be fed into the EZAudioPlot, EZAudioPlotGL, or whatever visualization you would like to do with the microphone data.
//   //dispatch_async(dispatch_get_main_queue(),^{
//    
//   // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        // Visualize this data brah, buffer[0] = left channel, buffer[1] = right channel
//        
//        if (sampleCount < 2)
//        {
//            if (sampleCount == 0)
//            {
//                free(sampleBuffer);
//                sampleBuffer = (float *)malloc(2048 * sizeof(float));
//                [self appendBufferAtIndex:0 buffer:buffer[0]];
//            }
//            else
//            {
//                [self appendBufferAtIndex:sampleCount * bufferSize buffer:buffer[0]];
//            }
//            
//            sampleCount ++;
//        }
//        else
//        {
//            sampleCount = 0;
//            
//            float soundPressureLevel = [self getSoundPressureLevel:sampleBuffer];
//            
//            if (soundPressureLevel > 25)
//            {
//                float pitch  =  getPitch(2048,44100,sampleBuffer);
//                
//                if ((isPitched > 0) && (pitchProbability > 0.95))
//                {
//                    
//                    lastPitch = pitchValid;
//                    pitchValid = pitch;
//                    
//                    //Atenua o erro de salto de oitavas
//                    self.microphone.microphoneOn = NO;
//                    double  delayInSeconds = 0.5;
//                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                        self.microphone.microphoneOn = YES;
//                    });
//
//
//                }
//            }
//            
//        }
//   // });
//}


- (void) microphone:(EZMicrophone *)microphone
   hasAudioReceived:(float **)buffer
     withBufferSize:(UInt32)bufferSize
withNumberOfChannels:(UInt32)numberOfChannels {
    
    
    if (sampleCount < 2)
    {
        TPCircularBufferProduceBytes(&(self->circularBuffer), buffer[0], 1024 * sizeof(float));
        sampleCount ++;
    }
    else
    {
        sampleCount = 0;
        [self calculatePitch];
    }

  //  TPCircularBufferProduceBytes(&(self->circularBuffer), buffer[0], 1024 * sizeof(float));
//        int32_t availableBytes = 0;
//        float *head = TPCircularBufferHead(&(self->circularBuffer), &availableBytes);
//        memcpy(head,buffer,2048 * sizeof(float));//copies samples to head
//        TPCircularBufferProduce(&(self->circularBuffer),2048 * sizeof(float)); //moves buffer head "forward in the circle"
    
}


//- (void)appendBufferAtIndex:(int)index buffer:(float *) newSample
//{
//    for (int i = index; i < bufferSize; i++) {
//        sampleBuffer[i] = newSample[i - index];
//    }
//}


void difference (float *inputBuffer, float *yinBuffer, int bufferSize){
    int bufferSize2 = (int) bufferSize/2;
    int j, tau;
    float delta;
    
    for (tau = 0; tau < bufferSize2; tau++) {
        yinBuffer[tau] = 0;
    }
    for (tau = 1; tau < bufferSize2; tau++) {
        for (j = 0; j < bufferSize2; j++) {
            delta = inputBuffer[j] - inputBuffer[j+tau];
            yinBuffer[tau] += delta * delta;
        }
    }
}

void cummulativeMeanNormalizedDifference(float *yinBuffer, int bufferSize){
    int bufferSize2 = (int) bufferSize/2;
    int tau;
    
    yinBuffer[0] = 1;
    //Very small optimization in comparison with AUBIO
    //start the running sum with the correct value:
    //the first value of the yinBuffer
    
    float runningSum = yinBuffer[1];
    
    //yinBuffer[1] is always 1
    yinBuffer[1] = 1;
    
    //now start at tau = 2
    for (tau = 2; tau < bufferSize2; tau++) {
        runningSum += yinBuffer[tau];
        yinBuffer[tau] *= tau / runningSum;
    }
}

int absoluteThreshold(float *yinBuffer, int bufferSize){
    int bufferSize2 = (int) bufferSize/2;
    double threshold = 0.15;
    int tau;
    
    for (tau = 1; tau < bufferSize2; tau++){
        if  (yinBuffer[tau] < threshold) {
            while (tau+1 < bufferSize2 && yinBuffer[tau+1] < yinBuffer[tau]) tau++;
            
            pitchProbability = (1 - yinBuffer[tau]);
            isPitched = true;
            return tau;
        }
    }
    
    
    isPitched = false;
    pitchProbability = 0;
    return -1;
}

float parabolicInterpolation(int tauEstimate, float *yinBuffer, int bufferSize){
    int bufferSize2 = (int) bufferSize/2;
    float s0, s1, s2;
    int x0 = (tauEstimate < 1) ? tauEstimate : tauEstimate -1;
    int x2 = (tauEstimate + 1 < bufferSize2) ? tauEstimate + 1 : tauEstimate;
    if (x0 == tauEstimate)
        return (yinBuffer[tauEstimate] <= yinBuffer[x2]) ? tauEstimate : x2;
    if (x2 == tauEstimate)
        return (yinBuffer[tauEstimate] <= yinBuffer[x0]) ? tauEstimate : x0;
    s0 = yinBuffer[x0];
    s1 = yinBuffer[tauEstimate];
    s2 = yinBuffer[x2];
    return tauEstimate + 0.5f * (s2 - s0) / (2.0f * s1 - s2 - s0);
}

float getPitch(UInt32 inNumberFrames, float sampleRate, float *inData){
    
    pitchProbability = 0;
    int bufferSize = inNumberFrames;
    
    float *inputBuffer = (void *) malloc(bufferSize * sizeof(float));
    float *yinBuffer = (void *) malloc((bufferSize/2) * sizeof(float));
    
    int tauEstimate = -1;
    float pitchInHertz = 0;
    
    // Setp 0: Get the data
    for (int i = 0; i < bufferSize; i++) {
        inputBuffer[i] = inData[i]/(32768); // 2^15 = 32768
    }
    
    // Step 2: Difference
    difference(inputBuffer, yinBuffer, bufferSize);
    
    // Step 3: cumulativeMeanNormalizedDifference
    cummulativeMeanNormalizedDifference(yinBuffer, bufferSize);
    // The cumulative mean normalized difference function
    // as described in step 3 of the YIN paper [1]
    
    // Step 4: absoluteThreshold
    tauEstimate = absoluteThreshold(yinBuffer, bufferSize);
    
    // Step 5: parabolicInterpolation
    if (tauEstimate !=-1) {
        float betterTau = parabolicInterpolation(tauEstimate, yinBuffer, bufferSize);
        pitchInHertz = sampleRate/betterTau;
    }
    
    return pitchInHertz;
}

- (double) getSoundPressureLevel:(float *) sampleBuffer
{
    double power = 0.0;
    
    
    for (int index = 0; index < bufferSize; index++) {
        power +=  sampleBuffer[index] *  sampleBuffer[index];
    }
    
    double mean = power / bufferSize;
    
    double rms  = sqrt(mean);
    
   // double value = pow(power,0.5) / bufferSize;
    
    return 20.0 * log10(rms) + 74.0;
}

                         
                         
- (void) calculatePitch
{
    
    int32_t available;
    float *tail = TPCircularBufferTail(&(self->circularBuffer), &available);
    
    if (tail == NULL)
        return;
    
    int samplesInBuffer = available / sizeof(float); //mono

    TPCircularBufferConsume(&(self->circularBuffer), samplesInBuffer * sizeof(float)); // moves tail forward
    
    float soundPressureLevel = [self getSoundPressureLevel:tail];
    
    if (soundPressureLevel > 28)
    {
        float pitch  =  getPitch(bufferSize,sampleRate,tail);
        
        if ((isPitched > 0) && (pitchProbability > 0.95))
        {
            
            lastPitch = pitchValid;
            pitchValid = pitch;
            
            //Atenua o erro de salto de oitavas
            
            [[EZMicrophone sharedMicrophone] stopFetchingAudio];
            double  delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [[EZMicrophone sharedMicrophone] startFetchingAudio];
            });
            
            dispatch_async(dispatch_get_main_queue(),^{
                [self updateInterface];
            });
            
        }
    }
    
}

- (void) updateInterface
{
    
    if ((isRecording) && (isPitched))
    {
        
        
        float frequency = 440.0;
        int A4_INDEX = 57;
        float r = pow(2.0, 1.0/12.0);
        float cent = pow(2.0, 1.0/1200.0);
        int r_index = 0;
        int cent_index = 0;
        
        
        
        if(pitchValid >= frequency)
        {
            while(pitchValid >= r*frequency)
            {
                frequency = r*frequency;
                r_index++;
            }
            
            while(pitchValid > cent*frequency)
            {
                frequency = cent*frequency;
                cent_index++;
            }
            
            if((cent*frequency - pitchValid) < (pitchValid - frequency))
                cent_index++;
            
            if(cent_index > 50)
            {
                r_index++;
                cent_index = 100 - cent_index;
                
                if (cent_index != 0)
                    cent_index = cent_index * -1;
            }
        }
        else
        {
            while(pitchValid <= frequency/r)
            {
                frequency = frequency/r;
                r_index--;
            }
            
            while(pitchValid < frequency/cent)
            {
                frequency = frequency/cent;
                cent_index++;
            }
            
            if((pitchValid - frequency/cent) < (frequency - pitchValid))
                cent_index++;
            
            if(cent_index >= 50)
            {
                r_index--;
                cent_index = 100 - cent_index;
            }
            else
            {
                if (cent_index != 0)
                    cent_index = cent_index * -1;
            }
            
        }
        
        
        
        int noteIndex = A4_INDEX + r_index;

        
        //Corrige o erro de salto de oitavas para frequências baixas
        if (noteIndex <= 30)
        {
            lastNoteIndex = noteIndex;
            lastPitchTime = [NSDate date];
        }
        else
        {
            if ((lastPitchTime) && (noteIndex > lastNoteIndex))
            {
                NSTimeInterval timeInterval = [lastPitchTime timeIntervalSinceNow];
                double seconds = fabs(timeInterval);
                
                if (seconds <= 4)
                {
                    int diference = noteIndex - lastNoteIndex;
                    
                    if (diference == 12)
                        return;
                }
                else
                {
                    lastPitchTime = nil;
                    lastNoteIndex = 0;
                }
                
            }
            else
            {
                lastPitchTime = nil;
                lastNoteIndex = 0;
            }
            
        }
        
        
        NSString *resultNote = noteNames[noteIndex];
        
        
        UIFont *smallFont = [UIFont fontWithName:@"Montserrat-Regular" size:10];
        
        [_frequency setFont:smallFont];
        [_cents setFont:smallFont];
        
        UIFont *defaultFont = [UIFont fontWithName:@"Oswald" size:14];
        UIFont *octaveFont = [UIFont fontWithName:@"Oswald" size:12];
        
        
        
        NSMutableAttributedString *noteString = [[NSMutableAttributedString alloc] initWithString:resultNote];
        [noteString addAttribute:NSFontAttributeName value:defaultFont
                           range:NSMakeRange(0, noteString.length - 1)];
        
        [noteString addAttribute:NSFontAttributeName value:octaveFont
                           range:NSMakeRange(noteString.length - 1,1)];
        
        
        [_frequencyValue setFont:defaultFont];
        [_frequencyValue setText: [NSString stringWithFormat:@"%.02f", pitchValid]];
        [_noteValue setAttributedText:noteString];
        
        
        [_centsValue setFont:defaultFont];
        
        
        float ratio =  (noteIndex / 12.0);
        float aux = ratio - floor(ratio);
        
        if (cent_index > 0)
            [_centsValue setText: [NSString stringWithFormat:@"+%d", cent_index]];
        else
            [_centsValue setText: [NSString stringWithFormat:@"%d", cent_index]];
        
        currentAngle = (int) (round(aux * 12) * 30) + (360 * cent_index / 1200);
        
        animation.fromValue = @(DEGREES_TO_RADIANS(lastAngle));
        animation.toValue = @(DEGREES_TO_RADIANS(currentAngle));
        animation.duration = 1.0f;
        animation.repeatCount = 0;
        animation.fillMode = kCAFillModeForwards;
        animation.removedOnCompletion = NO;
        
        
        if (lastAngle != currentAngle)
        {
            [_indicator.layer removeAllAnimations];
            [_indicator.layer addAnimation:animation forKey:@"rotation"];
            
            [_arrow.layer removeAllAnimations];
            [_arrow.layer addAnimation:animation forKey:@"rotation"];
            lastAngle = currentAngle;
        }
        
    }
    
}

- (void)initInterface
{
    UIFont *smallFont = [UIFont fontWithName:@"Montserrat-Regular" size:10];
    
    [_frequency setFont:smallFont];
    [_cents setFont:smallFont];
    
    UIFont *defaultFont = [UIFont fontWithName:@"Oswald" size:14];
    UIFont *octaveFont = [UIFont fontWithName:@"Oswald" size:12];
    
    
    NSMutableAttributedString *noteString = [[NSMutableAttributedString alloc] initWithString:@"C"];
    [noteString addAttribute:NSFontAttributeName value:defaultFont range:NSMakeRange(0, [noteString length])];
    
    
    NSMutableAttributedString *octaveString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%i", 4]];
    
    [octaveString addAttribute:NSFontAttributeName value:octaveFont range:NSMakeRange(0, [octaveString length])];
    
    [noteString appendAttributedString:octaveString];
    
    
    [_frequencyValue setFont:defaultFont];
    [_frequencyValue setText: [NSString stringWithFormat:@"%.02f", 261.63]];
    [_noteValue setAttributedText:noteString];
    [_centsValue setFont:defaultFont];
    [_centsValue setText: [NSString stringWithFormat:@"+%.01f", 0.0]];
    
    [_buttonState setFont:defaultFont];
    [_buttonState setText:@"OFF"];
    
    
}


- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (NSString *) platformType:(NSString *)platform
{
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}


    

@end
